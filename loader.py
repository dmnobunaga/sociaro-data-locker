#  Created by Dmitry Panchev for Sociaro Inc.
#  For any question contact me at dmitry@sociaro.com
import requests
from datetime import date, datetime, timedelta
from locker import Locker

import os
app_id = ''
API_KEY = ''


class Downloader(object):
    def __init__(self):
        self.main_class = Locker() # Read config with locker
        self.config = self.main_class.config
        self.file_list = ""
        self.file_name = ""
        self.directory_in = self.config['base']['directory_in']
        self.report_type = 'in_app_events_report'
        self.params = {
            'app_id': self.config['base']['app_id'],
            'api_token': self.config['base']['api_token'],
            'from': datetime.today().date(),
            'to': datetime.today().date(),
        }
        self.limit = 10
        self.days = 0

    def get_file_name(self):
        self.file_name = '{}-{}-{}-to-{}.csv'.format(app_id, self.report_type, self.params['from'], self.params['to'])
        file_name_installs_report = '{}-{}-{}-to-{}.csv'.format \
            (app_id, "installs_report", self.params['from'], self.params['to'])
        file_name_in_app_events_report = '{}-{}-{}-to-{}.csv'.format \
            (app_id, "in_app_events_report", self.params['from'], self.params['to'])
        if '{}/{}'.format(self.directory_in, file_name_installs_report) in self.file_list and '{}/{}'.format(
                self.directory_in, file_name_in_app_events_report) in self.file_list:
                self.params['from'] = (datetime.today().date() - timedelta(days=self.days)).strftime('%Y-%m-%d')
                self.params['to'] = (datetime.today().date() - timedelta(days=self.days)).strftime('%Y-%m-%d')
                self.days += 1
                if self.days >= self.limit:
                    exit(code=1)
                return 0
        self.file_list = self.main_class.get_file_list()
        if '{}/{}'.format(self.directory_in, self.file_name) in self.file_list:
            if self.report_type == 'installs_report':
                self.report_type = 'in_app_events_report'
            else:
                self.report_type = 'installs_report'
            return 0

        return self.file_name

    def download(self):
            ### This part was particullary taken from https://support.appsflyer.com/hc/en-us/articles/207034346-Pull-APIs-Pulling-AppsFlyer-Reports-by-APIs#Scripts
            request_url = 'https://hq.appsflyer.com/export/{}/{}/v5'.format(app_id, self.report_type)
            res = requests.request('GET', request_url, params=self.params)
            if res.status_code != 200:
                if res.status_code == 404:
                    print('There is a problem with the request URL. Make sure that it is correct')
                    print("Exit")
                    exit(9)
                else:
                    print('There was a problem retrieving data: ', res.text)
                    exit(9)
            else:
                f = open(file='{}/{}'.format(self.directory_in, self.file_name), mode='w', newline='',
                         encoding="utf-8")
                f.write(res.text)
                f.close()

    def main(self):
        while(True):
            if self.get_file_name() != 0:
                self.download()
