#  Created by Dmitry Panchev for Sociaro Inc.
#  For any question contact me at dmitry@sociaro.com
from locker import Locker
from loader import Downloader


# Run Loader job first
d = Downloader()
d.main()


# Run Locker job
l = Locker()
l.process()