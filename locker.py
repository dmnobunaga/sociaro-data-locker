#  Created by Dmitry Panchev for Sociaro Inc.
#  For any question contact me at dmitry@sociaro.com
import json, glob, csv, os, hashlib, logging


encryption = False
aws = False
tqdm = False


def hash_value(value):
    m = hashlib.md5()
    m.update(value.encode())
    return m.hexdigest()

try:
    from tqdm import tqdm as t
    tqdm = True
except ImportError:
    print("Can't use tqdm for display progress bar.")
try:
    import boto3
    from boto3.session import Session
    from botocore.exceptions import ClientError


    class SyncToAWS(object):
        def __init__(self, config=None):
            self.session = Session(aws_access_key_id=config['aws'].get('ACCESS_KEY', ''),
                                   aws_secret_access_key=config['aws'].get('SECRET_KEY', ''))
            self.s3 = self.session.resource('s3')
            self.bucket = config['aws'].get('bucket', '')
            self.folder = config['base'].get('directory_out', '')

        def __upload_file(self, file_name, bucket, s3_client, object_name=None):
            """Upload a file to an S3 bucket

            :param file_name: File to upload
            :param bucket: Bucket to upload to
            :param object_name: S3 object name. If not specified then file_name is used
            :return: True if file was uploaded, else False
            """
            # If S3 object_name was not specified, use file_name
            if object_name is None:
                object_name = file_name
            # Upload the file
            try:
                s3_client.upload_file(file_name, bucket, object_name)
            except ClientError as e:
                print(e)
                return False
            return True

        def get_file_list(self):
            filelist = glob.glob(f"{self.folder}/*.csv")
            if tqdm:
                filelist = t(filelist, desc=f"\nUpload files to AWS3 in {self.bucket}")
            return filelist

        def sync_folder(self):
            s3bucket = self.s3.Bucket(self.bucket)
            s3client = self.session.client('s3')
            for file in [file for file in self.get_file_list() if file not in list(s3bucket.objects.all())]:
                self.__upload_file(file_name=os.path.basename(file), bucket=self.bucket, object_name=os.path.basename(file), s3_client=s3client)
            print("Done")


    aws = True
    # sync = SyncToAWS({})
    # sync.sync_folder(".")

except ImportError:
    aws = False
    print("Can't use upload to S3 function, install boto3 first")

try:
    from Crypto.Cipher import AES
    from Crypto import Random


    def encrypt_string(key, value):
        enc_key = hash_value(key)
        encryptor = AES.new(enc_key, AES.MODE_CTR, 16)
        if len(value) >= 16:
            encryptedtext = encryptor.encrypt(value)
            return encryptedtext
        else:
            return value

    encryption = True
except ImportError:
    encryption = False
    print("Can't use encryption function")


class Locker():
    def __read_config_from_file(self):
        with open('config.json', 'r') as outfile:
            config = json.load(outfile)
            self.config = config
            self.directory_in = config['base'].get('directory_in', '.')
            self.directory_out = config['base'].get('directory_out', './out')
            self.encode_data = config['data-lock'].get('encode_data', False)  # True/False
            self.encode_key = config['data-lock'].get('encode_key', '123')  # True/False
            self.hash_data = config['data-lock'].get('hash_data', True)  # True/False
            self.columns = config['data-lock'].get('columns', [])  # True/False
            self.ACCESS_KEY = config['aws'].get('ACCESS_KEY', "")
            self.SECRET_KEY = config['aws'].get('SECRET_KEY', "")
            self.aws_enabled = config['aws'].get('enabled', False)

            # Create dirs if not exist
            self.__create_dir_if_not_exist()

    def __create_dir_if_not_exist(self):
        if not os.path.isdir(self.directory_in):
            os.makedirs(self.directory_in)
        if not os.path.isdir(self.directory_out):
            os.makedirs(self.directory_out)

    def __init__(self):
        self.__read_config_from_file()

    def get_file_list(self):
        filelist = glob.glob(f"{self.directory_in}/*.csv")
        # if tqdm:
        #     filelist = t(filelist, desc=f"\nProcess files in {self.directory_in}")
        return filelist

    def process(self):
        for file in self.get_file_list():
            with open(file, 'r') as csvfile:
                reader = csv.DictReader(csvfile)
                with open(f"{self.directory_out}/{os.path.basename(file)}", 'w') as csvfile_out:
                    writer = csv.DictWriter(csvfile_out, fieldnames=reader.fieldnames)
                    writer.writeheader()
                    for row in reader:
                        for column in self.columns:
                            if self.encode_data == True and self.hash_data == False and encryption == True:
                                value = encrypt_string(self.encode_key, row[column])
                            elif self.encode_data == False and self.hash_data == True:
                                value = hash_value(row[column])
                            else:
                                raise RuntimeError('Misconfiguration params data-locker section in config.json')
                            row[column] = value
                        writer.writerow(row)
        if aws==True and self.aws_enabled:
            aws_sync = SyncToAWS(config=self.config)
            aws_sync.sync_folder()

#
# main = Main()
# main.process()
