# Sociaro Data Locker

**Sociaro Data Locker**

**How it works?**

Program take CSV file from the directory_in, hash chosen columns.
Then programm storing the result in folder directory_out and uploading hashed data to our S3 bucket.

**How to use it?**

1. You need python version 3.6 or above and pip packet manager.
2. Install packages from requirements.txt
~~~~
python3 -m pip install -r requirements.txt
~~~~
3. Customize config.json  base section:
~~~~
"base": {
    "directory_in": ".", // Directory where you store appsflyer raw csv data that you get with Appsflyer Pull Api.
    "directory_out": "./out" // Directory with output hashed data in csv format.
},
~~~~
In "data-lock" section you can choose columns which you want to hash(encrypt).
~~~~
"columns": ["AppsFlyer ID", "Android ID"]
~~~~
AWS section contains credentials which program use for upload hashed(encrypted) data to s3 bucket.


Make configuration of  cron task for periodic refresh data

As example 
~~~~
* 12 * * * cd /path/to/directory/with/program && /usr/bin/python3 main.py
~~~~
For any further question feel free to ask me at dmitry@sociaro.com